<?php
session_start();
$bdd = new PDO('mysql:host=127.0.0.1;dbname=mexfrance;charset=utf8', 'root', '');

if(isset($_SESSION['id']) AND !empty($_SESSION['id'])){

$msg = $bdd->prepare('SELECT * FROM message WHERE id_destinataire = ?');
$msg->execute(array($_SESSION['id']));
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Boîte de réception</title>
</head>
<body>
    <a href="envoi.php">Nouveau message</a><br /><br /><br />
    <h3>Votre boîte de réception: </h3>
    <?php while($m = $msg->fetch()) { 
    
        $p_exp = $bdd->prepare('SELECT pseudo FROM membre WHERE id = ?');
        $p_exp->execute(array($m['id_expediteur']));
        $p_exp = $p_exp->fetch();
        $p_exp = $p_exp['pseudo'];
    ?>
        <b><? $p_exp ?></b> vous a envoyé: <br />
        <?= nl2br($m['message']) ?><br />
        --------------------------<br />
        <?php } ?>
</body>
</html>
<?php } ?>

